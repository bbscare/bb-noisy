
import argparse
import sys
from termcolor import cprint
import numpy as np
import matplotlib.pyplot as plt

import utils
import model
import keras
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Train models")
    parser.add_argument("--config", "-c", help="Train config")
    parser.add_argument("--data", "-d", help="Data folder. Folder where we can find binu8 files.")
    args = parser.parse_args()
    if not args.config or not args.data:
        parser.print_help()
        sys.exit()

    data_folder = args.data

    attack_byte = 0

    config = utils.open_config(args.config)
    traces_train = utils.load_binu8_data(data_folder + "/" + config["train"]["traces"])
    labels_train = utils.load_binu8_labels(data_folder + "/" + config["train"]["labels"], attack_byte)
    traces_test = utils.load_binu8_data(data_folder + "/" + config["test"]["traces"])
    labels_test = utils.load_binu8_labels(data_folder + "/" + config["test"]["labels"], attack_byte)

    print("Traces shape:",  traces_train.shape)

    stub = utils.get_model_stub(attack_byte)
    model = keras.models.load_model(f"models/{stub}/trained_model")

    test_index = 3
    one_trace = traces_test[test_index]
    one_trace = np.expand_dims(one_trace, axis=0)

    prediction = model.predict(one_trace)

    # Create the figure and the axes
    fig, ax = plt.subplots()

    # On the same axes, plot the sine and cosine curves
    ax.plot(prediction[0], label='prediction')
    ax.plot(labels_test[test_index], label='label')

    # Add legend to make it clear which line is which
    ax.legend()

    # Show the plot
    plt.show()