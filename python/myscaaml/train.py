
import argparse
import sys
from termcolor import cprint
import numpy as np

import utils
import model
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Train models")
    parser.add_argument("--config", "-c", help="Train config")
    parser.add_argument("--data", "-d", help="Data folder. Folder where we can find binu8 files.")
    args = parser.parse_args()
    if not args.config or not args.data:
        parser.print_help()
        sys.exit()

    data_folder = args.data

    attack_byte = 0

    config = utils.open_config(args.config)
    traces_train = utils.load_binu8_data(data_folder + "/" + config["train"]["traces"])
    labels_train = utils.load_binu8_labels(data_folder + "/" + config["train"]["labels"], attack_byte)
    traces_test = utils.load_binu8_data(data_folder + "/" + config["test"]["traces"])
    labels_test = utils.load_binu8_labels(data_folder + "/" + config["test"]["labels"], attack_byte)

    print("Traces shape:",  traces_train.shape)

    model = model.get_model(traces_train.shape[1:], config)
    stub = utils.get_model_stub(attack_byte)
    cb = [
        ModelCheckpoint(monitor="val_loss",
                        filepath=f"models/{stub}",
                        save_best_only=True),
        TensorBoard(log_dir="logs/" + stub, update_freq="batch")
    ]

    model.fit(  traces_train,
                labels_train,
                validation_data=(traces_test, labels_test),
                verbose=1,
                epochs=config["epochs"],
                callbacks=cb)

    model.save(f"models/{stub}/trained_model")
