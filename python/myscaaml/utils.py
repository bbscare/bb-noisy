# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Utils common to various SCAAML components"""

from tqdm.auto import tqdm
import tensorflow as tf
import numpy as np
import time
from random import randint
from termcolor import cprint
import json
from tensorflow.keras.utils import to_categorical

def open_config(config_path):
    with open(config_path, encoding="utf-8") as config_file:
        return json.loads(config_file.read())

def load_binu8_data(filepath):
    with open(filepath, 'rb') as f:
        traces_len = np.fromfile(f, dtype=np.uint32, count=1)[0]
        traces_count = np.fromfile(f, dtype=np.uint32, count=1)[0]
        data = np.fromfile(f, dtype=np.uint8)
    data = data.reshape((traces_count, traces_len, 1))
    return data

def load_binu8_labels(filepath, select_label):
    with open(filepath, 'rb') as f:
        labels_len = np.fromfile(f, dtype=np.uint32, count=1)[0]
        labels_count = np.fromfile(f, dtype=np.uint32, count=1)[0]
        labels = np.fromfile(f, dtype=np.uint8)
    labels = labels.reshape((labels_count, labels_len))
    labels = labels[:, select_label]

    # If you have more than 2 classes, use one-hot encoding for the labels
    num_classes = len(np.unique(labels))
    labels = to_categorical(labels, num_classes=num_classes)
    labels = tf.convert_to_tensor(labels, dtype="uint8")

    return labels


def pretty_hex(val):
    "convert a value into a pretty hex"
    s = hex(int(val))
    s = s[2:]  # remove 0x
    if len(s) == 1:
        s = "0" + s
    return s.upper()


def bytelist_to_hex(lst: list, spacer: str = " ") -> str:
    h = []

    for e in lst:
        h.append(pretty_hex(e))
    return spacer.join(h)


def hex_display(lst, prefix="", color="green"):
    "display a list of int as colored hex"
    h = []
    if len(prefix) > 0:
        prefix += "\t"
    for e in lst:
        h.append(pretty_hex(e))
    cprint(prefix + " ".join(h), color)


def get_model_stub(attack_byte):
    return (f"byte_{attack_byte}")


def get_target_stub(config):
    return f"{config['device']}-{config['algorithm']}"


def get_num_gpu():
    return len(tf.config.list_physical_devices("GPU"))


def tf_cap_memory():
    gpus = tf.config.experimental.list_physical_devices("GPU")

    if gpus:
        for gpu in gpus:
            try:
                tf.config.experimental.set_memory_growth(gpu, True)
            except RuntimeError as e:
                # Memory growth must be set before GPUs have been initialized
                print(e)






def display_config(config_name, config):
    """Pretty print a config object in terminal.

    Args:
        config_name (str): name of the config
        config (dict): config to display
    """
    cprint(f"[{config_name}]", "magenta")
    cnt = 1
    for k, v in config.items():
        if cnt % 2:
            color = "cyan"
        else:
            color = "yellow"
        cprint(f"{k}:{v}", color)
        cnt += 1


def from_categorical(predictions):
    "reverse of categorical"
    # note: doing it as a list is significantly faster than a single argmax
    return [np.argmax(p) for p in predictions]
