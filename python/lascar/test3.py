import argparse
import sys
from termcolor import cprint
import numpy as np

import utils
from aes import encrypt_one_round
import matplotlib.pyplot as plt
# from sklearn.neural_network import MLPClassifier
from sklearn.discriminant_analysis import *
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.covariance import LedoitWolf
from sklearn.decomposition import PCA
import joblib

def mean_by_label(traces, labels):
    
    # Initialize an empty matrix for storing the mean traces
    mean_traces = np.zeros((256, traces.shape[1]))

    # Loop over all possible labels (0 to 255)
    for i in range(256):
        # Get the indices of the rows with the current label
        indices = np.where(labels == i)[0]

        # If there are no such rows, continue to the next label
        if len(indices) == 0:
            continue

        # Calculate the mean trace for the current label and store it
        mean_traces[i] = np.mean(traces[indices], axis=0)

    return mean_traces



def ComputeEndRound1(plaintexts,key,o):
    (n, _) = plaintexts.shape
    P = np.zeros(n, dtype=np.uint8)
    for i in range(n):
        P[i] = encrypt_one_round(plaintexts[i,:], key)[o]

    return P


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Train models")
    parser.add_argument("--config", "-c", help="Train config")
    parser.add_argument("--data", "-d", help="Data folder. Folder where we can find binu8 files.")
    args = parser.parse_args()
    if not args.config or not args.data:
        parser.print_help()
        sys.exit()

    data_folder = args.data

    attack_byte = 0

    config = utils.open_config(args.config)
    traces_count = 1000000
    traces_train = utils.load_binu8_data_count(data_folder + "/" + config["train"]["traces"], traces_count)
    labels_train = utils.load_binu8_labels_count(data_folder + "/" + config["train"]["labels"], traces_count)
    traces_test = utils.load_binu8_data(data_folder + "/" + config["test"]["traces"])
    labels_test = utils.load_binu8_labels(data_folder + "/" + config["test"]["labels"])

    # r = [36+16,48+16,60+16]
    # r = range(50,77)
    # traces_train = traces_train[:,r]
    # traces_test = traces_test[:,r]


    # traces_train = traces_train[range(traces_limit),:]
    # labels_train = labels_train[range(traces_limit),:]

    key = [0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c]    
    # labels_train = ComputeEndRound1(labels_train, key, attack_byte)
    # labels_test = ComputeEndRound1(labels_test, key, attack_byte)
    labels_train = labels_train[:,attack_byte]
    labels_test = labels_test[:,attack_byte]

    # traces_train = mean_by_label(traces_train, labels_train)
    # labels_train = np.arange(0, 256, 1)

    print("Training traces shape:", traces_train.shape)
    print("Training labels shape:", labels_train.shape)

    
    # pca = PCA(n_components=500)
    # pca.fit(traces_train)
    # joblib.dump(pca, 'pca.pkl') 


    pca = joblib.load("pca.pkl")
    traces_train = pca.transform(traces_train)

    fig, ax = plt.subplots()
    ax.plot(traces_train[0,:])
    plt.show()
    print("Post PCA training traces shape:", traces_train.shape)
    utils.save_binu8_data(data_folder + "/" + config["train"]["traces"] + ".pca", traces_train)

    # fig, ax = plt.subplots()
    # ax.plot(pca.explained_variance_ratio_)
    # # ax.plot(pca.singular_values_)
    # plt.show()

    
    
    # clf = QuadraticDiscriminantAnalysis()
    # clf = LinearDiscriminantAnalysis()
    # scaler = StandardScaler()
    # scaler.fit(traces_train)
    # scaler.transform(traces_train)
    # joblib.dump(scaler, 'scaler.pkl') 
    # clf.fit(traces_train, labels_train)

    
    # joblib.dump(clf, 'lda_model.pkl') 

    # scaler = joblib.load("scaler.pkl")
    # clf = joblib.load("lda_model.pkl")

    # # eval
    # # scaler.transform(traces_test)
    # r = clf.score(traces_test, labels_test)
    # print(r)


    # t = 234
    # print(clf.predict([traces_test[t,:]]))
    # print(labels_test[t])
    # onehot = np.zeros(256)
    # onehot[int(labels_test[t])] = 1.0
    # p = clf.predict_proba([traces_test[t,:]])[0]

    # fig, ax = plt.subplots()
    # ax.plot(p)
    # ax.plot(onehot)
    # plt.show()