import argparse
import sys
from termcolor import cprint
import numpy as np

import utils
from aes import encrypt_one_round
import matplotlib.pyplot as plt

from lascar.container import Trace, TraceBatchContainer
from lascar import *
from lascar.tools.aes import sbox

def ComputeEndRound1(plaintexts,key,o):
    (n, _) = plaintexts.shape
    P = np.zeros(n)
    for i in range(n):
        P[i] = encrypt_one_round(plaintexts[i,:], key)[o]

    return P


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Train models")
    parser.add_argument("--config", "-c", help="Train config")
    parser.add_argument("--data", "-d", help="Data folder. Folder where we can find binu8 files.")
    args = parser.parse_args()
    if not args.config or not args.data:
        parser.print_help()
        sys.exit()

    data_folder = args.data

    attack_byte = 0
    traces_count = 10000

    config = utils.open_config(args.config)
    traces_train = utils.load_binu8_data_count(data_folder + "/" + config["train"]["traces"], traces_count)
    labels_train = utils.load_binu8_labels_count(data_folder + "/" + config["train"]["labels"], traces_count)
    # traces_test = utils.load_binu8_data(data_folder + "/" + config["test"]["traces"])
    # labels_test = utils.load_binu8_labels(data_folder + "/" + config["test"]["labels"])

    key = [0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c]    
    # labels_train = ComputeEndRound1(labels_train, key, attack_byte)
    labels_train = labels_train[:,attack_byte]

    print("Training traces shape:", traces_train.shape)
    print("Training labels shape:", labels_train.shape)

    container = TraceBatchContainer(traces_train, labels_train)

    # def partition_function(value):  # partition_function must take 1 argument: the value returned by the container at each trace
    #     return int(value[0] == 0)  # "plaintext[3] == 0" versus "all other values"

    # ttest_engine = TTestEngine("ttest", partition_function)
    # plot_output = MatPlotLibOutputMethod(ttest_engine)
    # session = Session(container, output_method=plot_output)
    # session.add_engine(ttest_engine)

    # session.run(batch_size=2000)

    # guess_range = range(256)
    # def selection_function(value, guess):
    #     return sbox[value[0] ^ guess]

    guess_range = range(1)
    def selection_function(value, guess):
        return value
    cpa_engine = CpaEngine("cpa", selection_function, guess_range)

    session = Session(
        container, engine=cpa_engine
    )

    session.run(batch_size=200)

    results = cpa_engine.finalize()
    print(results.shape)
    print("Best guess is %02X." % results.max(1).argmax())

    import matplotlib.pyplot as plt

    plt.plot(results.T)
    plt.show()