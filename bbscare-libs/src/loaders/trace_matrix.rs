use std::path::{Path};
use std::io::{BufReader, Read, BufWriter, Write, SeekFrom, Seek};
use std::fs::File;

use rayon::prelude::{IntoParallelRefIterator, ParallelIterator};
use serde::{Serialize, Deserialize};

use crate::information::entropies::{EntropyEstimator, fractionnal_added_information_1to1, fractionnal_added_information_2to1};
use crate::information::hgraph::{HEdge, HEdgeInput};

use super::file_storable::FileStorable;

#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct SimpleFilteringResults {
    constants_keep_vector: Vec<bool>,
    identical_keep_vector: Vec<bool>,
}

impl SimpleFilteringResults {
    pub fn columns_before_filtering(&self) -> usize {
        self.constants_keep_vector.len()
    }

    pub fn columns_constants_removed(&self) -> usize {
        self.identical_keep_vector.len()
    }

    pub fn columns_constants_identical_removed(&self) -> usize {
        self.identical_keep_vector.iter().filter(|b|**b).count()
    }

    pub fn apply_keep_vector(line: Vec<u8>, keep_vector: &Vec<bool>) -> Vec<u8> {
        let mut new_line: Vec<u8> = Vec::new();
        
        for (i, v) in line.into_iter().enumerate() {
            if keep_vector[i] {
                new_line.push(v);
            }
        }

        new_line
    }


    pub fn apply_filtering(&self, lines: Vec<Vec<u8>>) -> Vec<Vec<u8>> {
        let mut filtered: Vec<Vec<u8>> = Vec::with_capacity(lines.len());
        let no_constant_size = self.columns_constants_removed();
        let no_identical_size = self.columns_constants_identical_removed();

        // let keep_no_constant = self.constants_keep_vector.iter().filter(|i|**i).count();
        // println!("Trace len {} ({})", lines[0].len(), self.constants_keep_vector.len());
        // println!("Keep no constant: {}", keep_no_constant);

        for line in lines {
            let no_constants: Vec<u8> = SimpleFilteringResults::apply_keep_vector(line, &self.constants_keep_vector);
            assert!(no_constants.len() == no_constant_size, "No constants size: {} vs expected {}", no_constants.len(), no_constant_size);
            
            let no_identical: Vec<u8> = SimpleFilteringResults::apply_keep_vector(no_constants, &self.identical_keep_vector);
            assert!(no_identical.len() == no_identical_size);
            filtered.push(no_identical);
        }

        filtered
    }

    
}

impl FileStorable for SimpleFilteringResults {}
pub struct TraceMatrixColumns {
    columns: Vec<Vec<u8>>
}

fn are_all_same(v: &[u8]) -> bool {
    if v.len() == 0 {
        return false;
    }
    else {
        let f = v[0];
        for e in v.iter() {
            if *e != f {
                return false;
            }
        }
        return true
    }
}

impl TraceMatrixColumns {
    pub fn from_file<P: AsRef<Path>>(path: P) -> anyhow::Result<TraceMatrixColumns> {
        let mut reader = BufReader::new(File::open(path)?);


        // Read file headers


        // len
        let mut int32buf: [u8; 4] = [0; 4];
        reader.read_exact(&mut int32buf)?;
        let trace_len: usize = u32::from_le_bytes(int32buf) as usize;
        // count
        reader.read_exact(&mut int32buf)?;
        let trace_count: usize = u32::from_le_bytes(int32buf) as usize;

        // Read traces, line by line
        let mut columns: Vec<Vec<u8>> = Vec::with_capacity(trace_len);

        for _ in 0..trace_len {
            let mut col: Vec<u8> = vec![0; trace_count];//Vec::with_capacity(trace_len);
            reader.read_exact(&mut col)?;
            columns.push(col);
        }

        return Ok(TraceMatrixColumns { columns });
    }

    pub fn count_uniques(&self, index: usize) -> usize {
        let mut v: Vec<u8> = self.column(index).to_vec();
        v.sort();
        v.dedup();
        v.len()
    }

    pub fn traces_count(&self) -> usize {
        if self.columns.len() > 0 {
            return self.columns[0].len();
        }
        else {
            return 0;
        }
    }

    pub fn min(&self) -> Vec<u8> {

        let minimums: Vec<u8> = self.columns.par_iter().map(|c| {
            c.iter().min().map(|b|*b).unwrap_or(0u8)
        }).collect();

        minimums
    }

    pub fn max(&self) -> Vec<u8> {

        let minimums: Vec<u8> = self.columns.par_iter().map(|c| {
            c.iter().max().map(|b|*b).unwrap_or(0u8)
        }).collect();

        minimums
    }

    pub fn column(&self, index: usize) -> &[u8] {
        &self.columns[index]
    }

    pub fn column_mut(&mut self, index: usize) -> &mut Vec<u8> {
        &mut self.columns[index]
    }

    pub fn write_column(&mut self, index: usize, new_column: Vec<u8>) {
        self.columns[index] = new_column;
    }

    pub fn traces_len(&self) -> usize {
        return self.columns.len();
    }

    pub fn from_lines(traces: &TraceMatrixLines) -> TraceMatrixColumns {

        let trace_len = traces.traces_len();
        let trace_count = traces.traces_count();
        let mut columns = Vec::with_capacity(trace_len);

        for c in 0..trace_len {
            let mut col: Vec<u8> = Vec::with_capacity(trace_count);
            for line in traces.lines.iter() {
                col.push(line[c]);
            }
            columns.push(col);
        }

        return TraceMatrixColumns { columns }
    }

    // remove a column if it is a repeated constant value
    fn remove_constants(&mut self) -> Vec<bool> {
        let mut keep_vector: Vec<bool> = Vec::with_capacity(self.columns.len());
        self.columns.retain(|v|{
            let keep = !are_all_same(v);
            keep_vector.push(keep);
            keep
        });
        return keep_vector;
    }

    // remove a column if an identical previous column exists
    fn remove_identical(&mut self) -> Vec<bool> {
        let mut stolen_columns: Vec<Vec<u8>> = self.columns.drain(..).collect(); // rev required, since we pop after
        let mut keep_vector: Vec<bool> = Vec::with_capacity(stolen_columns.len());
        
        while stolen_columns.len() > 0 {
            let investigated_column = stolen_columns.remove(0);
            let mut identical_found = false;
            for c in self.columns.iter() {
                if c == &investigated_column {
                    identical_found = true;
                    break;
                }
            }
            if !identical_found {
                self.columns.push(investigated_column);
            }
            keep_vector.push(!identical_found);
        }

        return keep_vector;
    }

    pub fn fai1(&self, i1: usize, i2: usize, estimator: &EntropyEstimator) -> anyhow::Result<f32> {
        fractionnal_added_information_1to1(self.column(i2), self.column(i1), estimator)
    }

    pub fn fai2(&self, i1: usize, i2: usize, i3: usize, estimator: &EntropyEstimator) -> anyhow::Result<f32> {
        fractionnal_added_information_2to1(self.column(i3), self.column(i1), self.column(i2), estimator)
    }

    pub fn fai_hedge(&self, edge: &HEdge, estimator: &EntropyEstimator) -> anyhow::Result<f32> {
        let i3 = edge.output;
        match edge.inputs {
            HEdgeInput::One(i1) => fractionnal_added_information_1to1(self.column(i3), self.column(i1), estimator),
            HEdgeInput::Two(i1, i2) => fractionnal_added_information_2to1(self.column(i3), self.column(i1), self.column(i2), estimator)
        }
    }

    pub fn simple_filtering(&mut self) -> SimpleFilteringResults {
        let constants_keep_vector = self.remove_constants();
        let identical_keep_vector = self.remove_identical();

        return SimpleFilteringResults { constants_keep_vector, identical_keep_vector };
    }

    fn apply_keep_vector(&mut self, keep_vector: &Vec<bool>) {
        let mut stolen_columns: Vec<Vec<u8>> = self.columns.drain(..).collect();
        
        let mut counter: usize = 0;
        while let Some(investigated_column) = stolen_columns.pop() {
            if keep_vector[counter] {
                self.columns.push(investigated_column);
            }

            counter += 1;
        }
    }

    pub fn apply_simple_filtering(&mut self, filtering: &SimpleFilteringResults) {
        self.apply_keep_vector(&filtering.constants_keep_vector);
        self.apply_keep_vector(&filtering.identical_keep_vector);
    }

    pub fn save_to_file<P: AsRef<Path>>(&self, path: P) -> anyhow::Result<()> {
        let mut writer = BufWriter::new(File::create(path)?);

        // len
        let len_bytes = (self.traces_len() as u32).to_le_bytes();
        writer.write(&len_bytes)?;
        // count
        let count_bytes = (self.traces_count() as u32).to_le_bytes();
        writer.write(&count_bytes)?;

        for col in self.columns.iter() {
            writer.write(&col)?;
        }

        return Ok(());
    }

    pub fn append_prefix(&mut self, prefix: &TraceMatrixColumns) {
        let mut stolen_columns: Vec<Vec<u8>> = self.columns.drain(..).collect();

        for c in prefix.columns.iter() {
            self.columns.push(c.clone());
        }

        for c in stolen_columns.drain(..) {
            self.columns.push(c);
        }
    }

    pub fn append_postfix(&mut self, postfix: &TraceMatrixColumns) {
        for c in postfix.columns.iter() {
            self.columns.push(c.clone());
        }
    }

    pub fn find_inputs(&self, input_matrix: &TraceMatrixColumns, noise_threshold: f32, estimator: &EntropyEstimator) -> anyhow::Result<Vec<usize>> {
        let mut input_times: Vec<usize> = vec![usize::MAX; input_matrix.traces_len()];

        for  (i,i_vec) in input_matrix.columns.iter().enumerate() {
            let mut best_candidate_time: usize = usize::MAX;
            let mut best_candidate_el_count: usize = 0;
            for (t, t_vec) in self.columns.iter().enumerate() {
                if fractionnal_added_information_1to1(t_vec, i_vec,estimator)? < noise_threshold {
                    let el_count = count_uniques(t_vec);
                    if el_count > best_candidate_el_count {
                        best_candidate_time = t;
                        best_candidate_el_count = el_count;
                    }
                }
            }
            input_times[i] = best_candidate_time;           
        }

        Ok(input_times)
    }

    // like find_inputs, but start looking by the end
    pub fn find_outputs(&self, output_matrix: &TraceMatrixColumns, noise_threshold: f32, estimator: &EntropyEstimator) -> anyhow::Result<Vec<usize>> {
        let mut output_times: Vec<usize> = vec![usize::MAX;output_matrix.traces_len()];

        for (i,i_vec) in output_matrix.columns.iter().enumerate() {
            let mut best_candidate_time: usize = usize::MAX;
            let mut best_candidate_el_count: usize = 0;
            for (t, t_vec) in self.columns.iter().enumerate().rev() {
                if fractionnal_added_information_1to1(t_vec, i_vec,estimator)? < noise_threshold {
                    let el_count = count_uniques(t_vec);
                    if el_count > best_candidate_el_count {
                        best_candidate_time = t;
                        best_candidate_el_count = el_count;
                    }
                }
            }
            output_times[i] = best_candidate_time;
        }

        Ok(output_times)
    }
}

fn count_uniques(vec: &[u8]) -> usize {
    let mut v: Vec<u8> = vec.to_vec();
    v.sort();
    v.dedup();
    v.len()
}

// impl Index<usize> for TraceMatrixColumns {
//     type Output = Vec<u8>;

//     fn index(&self, index: usize) -> &Self::Output {
//         self.columns.index(index)
//     }
// }
pub struct TraceMatrixLines {
    pub lines: Vec<Vec<u8>>
}

impl TraceMatrixLines {

    pub fn from_columns(traces: &TraceMatrixColumns) -> TraceMatrixLines {

        let trace_len = traces.traces_len();
        let trace_count = traces.traces_count();
        let mut lines = Vec::with_capacity(trace_count);

        for l in 0..trace_count {
            let mut line: Vec<u8> = Vec::with_capacity(trace_len);
            for col in traces.columns.iter() {
                line.push(col[l]);
            }
            lines.push(line);
        }

        return TraceMatrixLines { lines }
    }

    pub fn from_file_truncated<P: AsRef<Path>>(path: P, traces_count: usize) -> anyhow::Result<TraceMatrixLines> {
        let mut reader = BufReader::new(File::open(path)?);


        // Read file headers

        // len
        let mut int32buf: [u8; 4] = [0; 4];
        reader.read_exact(&mut int32buf)?;
        let trace_len: usize = u32::from_le_bytes(int32buf) as usize;
        // count
        reader.read_exact(&mut int32buf)?;
        let true_trace_count: usize = u32::from_le_bytes(int32buf) as usize;

        assert!(traces_count <= true_trace_count);

        // Read traces, line by line
        let mut lines: Vec<Vec<u8>> = Vec::with_capacity(traces_count);

        for _ in 0..traces_count {
            let mut line: Vec<u8> = vec![0; trace_len];//Vec::with_capacity(trace_len);
            reader.read_exact(&mut line)?;
            lines.push(line);
        }

        return Ok(TraceMatrixLines { lines });
    }

    pub fn from_file<P: AsRef<Path>>(path: P) -> anyhow::Result<TraceMatrixLines> {
        let mut reader = BufReader::new(File::open(path)?);


        // Read file headers

        // len
        let mut int32buf: [u8; 4] = [0; 4];
        reader.read_exact(&mut int32buf)?;
        let trace_len: usize = u32::from_le_bytes(int32buf) as usize;
        // count
        reader.read_exact(&mut int32buf)?;
        let trace_count: usize = u32::from_le_bytes(int32buf) as usize;

        // Read traces, line by line
        let mut lines: Vec<Vec<u8>> = Vec::with_capacity(trace_count);

        for _ in 0..trace_count {
            let mut line: Vec<u8> = vec![0; trace_len];//Vec::with_capacity(trace_len);
            reader.read_exact(&mut line)?;
            lines.push(line);
        }

        return Ok(TraceMatrixLines { lines });
    }

    pub fn keep_only_firsts(&mut self, to_keep: usize) {
        self.lines.drain(to_keep..);
    }

    pub fn traces_len(&self) -> usize {
        if self.lines.len() > 0 {
            return self.lines[0].len();
        }
        else {
            return 0;
        }
    }

    pub fn traces_count(&self) -> usize {
        return self.lines.len();
    }

    pub fn write_to_file<P: AsRef<Path>>(&self, path: P) -> anyhow::Result<()> {
        let mut writer = BufWriter::new(File::create(path)?);

        // len
        let len_bytes = (self.traces_len() as u32).to_le_bytes();
        writer.write(&len_bytes)?;
        // count
        let count_bytes = (self.traces_count() as u32).to_le_bytes();
        writer.write(&count_bytes)?;

        for line in self.lines.iter() {
            writer.write(&line)?;
        }

        return Ok(());
    }
}

pub struct TraceMatrixLinesJointBatchReader {
    traces_reader: BufReader<File>,
    inputs_reader: BufReader<File>,
    outputs_reader: BufReader<File>,
    pub traces_len: usize,
    pub inputs_len: usize,
    pub outputs_len: usize,
    current_trace: usize,
    pub traces_count: usize,
    pub batch_size: usize,
}

pub struct TraceMatrixLinesBatchReader {
    reader: BufReader<File>,
    pub traces_len: usize,
    current_trace: usize,
    pub traces_count: usize,
    pub batch_size: usize,
}

pub struct TraceMatrixLinesBatchWriter {
    writer: BufWriter<File>,
    pub traces_len: usize,
    current_trace: usize,
    pub traces_count: usize,
}

impl TraceMatrixLinesJointBatchReader {
    pub fn new<P: AsRef<Path>>(traces_path: P, inputs_path: P, outputs_path: P, batch_size: usize) -> anyhow::Result<TraceMatrixLinesJointBatchReader> {
        let mut traces_reader = BufReader::new(File::open(traces_path)?);


        // Read traces headers
        // len
        let mut int32buf: [u8; 4] = [0; 4];
        traces_reader.read_exact(&mut int32buf)?;
        let traces_len: usize = u32::from_le_bytes(int32buf) as usize;
        // count
        traces_reader.read_exact(&mut int32buf)?;
        let traces_count: usize = u32::from_le_bytes(int32buf) as usize;

        if traces_count % batch_size != 0 {
            return Err(anyhow::format_err!("Traces count is not a multiple of batch size."));
        }

        // inputs
        let mut inputs_reader = BufReader::new(File::open(inputs_path)?);
        inputs_reader.read_exact(&mut int32buf)?;
        let inputs_len: usize = u32::from_le_bytes(int32buf) as usize;
        // count
        inputs_reader.read_exact(&mut int32buf)?;
        let inputs_count: usize = u32::from_le_bytes(int32buf) as usize;

        // outputs
        let mut outputs_reader = BufReader::new(File::open(outputs_path)?);
        outputs_reader.read_exact(&mut int32buf)?;
        let outputs_len: usize = u32::from_le_bytes(int32buf) as usize;
        // count
        outputs_reader.read_exact(&mut int32buf)?;
        let outputs_count: usize = u32::from_le_bytes(int32buf) as usize;

        assert!(inputs_count == traces_count);
        assert!(outputs_count == traces_count);

        Ok(TraceMatrixLinesJointBatchReader { traces_reader, inputs_reader, outputs_reader, traces_len, inputs_len, outputs_len, current_trace: 0, traces_count, batch_size })
    }

    pub fn read_batch(&mut self) -> anyhow::Result<Vec<Vec<u8>>> {
        let mut lines: Vec<Vec<u8>> = Vec::with_capacity(self.batch_size);
        
        let total_len = self.inputs_len + self.traces_len + self.outputs_len;

        for _ in 0..self.batch_size {

            let mut input: Vec<u8> = vec![0; self.inputs_len];
            let mut trace: Vec<u8> = vec![0; self.traces_len];
            let mut output: Vec<u8> = vec![0; self.outputs_len];
            self.inputs_reader.read_exact(&mut input)?;
            self.traces_reader.read_exact(&mut trace)?;
            self.outputs_reader.read_exact(&mut output)?;

            let mut line = Vec::with_capacity(total_len);
            line.extend(input);
            line.extend(trace);
            line.extend(output);

            lines.push(line);
            self.current_trace += 1;
        } 

        Ok(lines)
    }

    // (input batch, trace batch, output batch)
    pub fn read_batches(&mut self) -> anyhow::Result<(Vec<Vec<u8>>, Vec<Vec<u8>>, Vec<Vec<u8>>)> { 
        let mut inputs: Vec<Vec<u8>> = Vec::with_capacity(self.batch_size);
        let mut traces: Vec<Vec<u8>> = Vec::with_capacity(self.batch_size);
        let mut outputs: Vec<Vec<u8>> = Vec::with_capacity(self.batch_size);
        
        for _ in 0..self.batch_size {

            let mut input: Vec<u8> = vec![0; self.inputs_len];
            let mut trace: Vec<u8> = vec![0; self.traces_len];
            let mut output: Vec<u8> = vec![0; self.outputs_len];
            self.inputs_reader.read_exact(&mut input)?;
            self.traces_reader.read_exact(&mut trace)?;
            self.outputs_reader.read_exact(&mut output)?;

            inputs.push(input);
            traces.push(trace);
            outputs.push(output);

            self.current_trace += 1;
        } 

        Ok((inputs, traces, outputs))
    }

    pub fn steps(&self) -> usize {
        self.traces_count / self.batch_size
    }

    pub fn is_terminated(&self) -> bool {
        self.current_trace == self.traces_count
    }
}


impl TraceMatrixLinesBatchReader {
    pub fn new<P: AsRef<Path>>(path: P, batch_size: usize) -> anyhow::Result<TraceMatrixLinesBatchReader> {
        let mut reader = BufReader::new(File::open(path)?);


        // Read traces headers
        // len
        let mut int32buf: [u8; 4] = [0; 4];
        reader.read_exact(&mut int32buf)?;
        let traces_len: usize = u32::from_le_bytes(int32buf) as usize;
        // count
        reader.read_exact(&mut int32buf)?;
        let traces_count: usize = u32::from_le_bytes(int32buf) as usize;

        if traces_count % batch_size != 0 {
            return Err(anyhow::format_err!("Traces count is not a multiple of batch size."));
        }


        Ok(TraceMatrixLinesBatchReader { reader, traces_len, current_trace: 0, traces_count, batch_size })
    }

    pub fn read_batch(&mut self) -> anyhow::Result<Vec<Vec<u8>>> {
        let mut lines: Vec<Vec<u8>> = Vec::with_capacity(self.batch_size);

        for _ in 0..self.batch_size {

            let mut trace: Vec<u8> = vec![0; self.traces_len];
            self.reader.read_exact(&mut trace)?;
             lines.push(trace);
            self.current_trace += 1;
        } 

        Ok(lines)
    }

    pub fn steps(&self) -> usize {
        self.traces_count / self.batch_size
    }

    pub fn is_terminated(&self) -> bool {
        self.current_trace == self.traces_count
    }

    pub fn reset(&mut self) -> anyhow::Result<()> {
        self.current_trace = 0;
        self.reader.get_mut().seek(SeekFrom::Start(8))?;
        Ok(())
    }
}

impl TraceMatrixLinesBatchWriter {
    pub fn new<P: AsRef<Path>>(path: P, traces_len: usize, traces_count: usize) -> anyhow::Result<TraceMatrixLinesBatchWriter> {
        let mut writer = BufWriter::new(File::create(path)?);

        // len
        let len_bytes = (traces_len as u32).to_le_bytes();
        writer.write(&len_bytes)?;
        // count
        let count_bytes = (traces_count as u32).to_le_bytes();
        writer.write(&count_bytes)?;

        Ok(TraceMatrixLinesBatchWriter { writer, traces_len, current_trace: 0, traces_count })
    }

    pub fn write_batch(&mut self, batch: &Vec<Vec<u8>>) -> anyhow::Result<()> {
        self.current_trace += batch.len();
        if self.current_trace > self.traces_count {
            return Err(anyhow::format_err!("Too many traces written in file."));
        }

        for line in batch.iter() {
            assert!(line.len() == self.traces_len, "{} vs expected {}", line.len(), self.traces_len);
            self.writer.write(&line)?;
        }  

        Ok(())
    }
}


#[cfg(test)]
mod tests {
    use crate::loaders::trace_matrix::*;

    #[test]
    fn test_load_traces_columns() {
        let traces_lines = TraceMatrixLines::from_file("../data/traces_raw.binu8").unwrap();
        assert_eq!(traces_lines.traces_count(), 100_000);

        let traces_columns = TraceMatrixColumns::from_file("../data/poor_traces_if.binu8").unwrap();
        assert_eq!(traces_columns.traces_count(), 50_000);
    }
}