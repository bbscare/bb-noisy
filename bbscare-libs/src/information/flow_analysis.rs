use std::ops::Index;
use std::path::Path;
use std::{cmp::min, cmp::max};
use std::fs::File;
use std::io::Write;

use std::collections::HashMap;
// use rayon::prelude::IntoParallelIterator;
use serde::{Serialize, Deserialize};
use npyz::WriterBuilder;

use rayon::iter::{ParallelIterator, IntoParallelRefIterator};


use crate::information::entropies::fractionnal_added_information_2to1;
use crate::loaders::file_storable::FileStorable;
use crate::loaders::trace_matrix::TraceMatrixColumns;
use super::entropies::{fractionnal_added_information_1to1, EntropyEstimator, shannon_entropy2, fractionnal_added_information_2to1_memoize_knowing};
use super::hgraph::{HGraph, HEdge};

#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct FaiMatrix {
    pub values: HashMap<(usize, usize), f32>, // (i1, i2) -> entropy. i1 < i2 and values[(i1, i2)] == values[(i2, i1)]
    pub len: usize,
}

impl FaiMatrix {
    // parallel implementation
    pub fn from_traces(traces: &TraceMatrixColumns, estimator: &EntropyEstimator, mut window: usize) -> anyhow::Result<FaiMatrix> {
        if window == 0 {
            window = traces.traces_len();
        }
    
        let i1range: Vec<usize> = (0..(traces.traces_len()-1)).collect(); 
        let map_results = i1range.par_iter().map(|pi1| {
            let i1 = *pi1;
        
            let max_investigate = min(i1+window, traces.traces_len());
            let mut local_values: HashMap<(usize, usize), f32> = HashMap::with_capacity(window);
            for i2 in (i1+1)..max_investigate {
                let fai = fractionnal_added_information_1to1(traces.column(i2), traces.column(i1), estimator).unwrap();
                local_values.insert((i1, i2), fai);
                local_values.insert((i2,i1), fai);
            }
            local_values
        });

        // reduce
        let values: HashMap<(usize, usize), f32> = map_results.reduce(
            || { HashMap::new() }, 
            |mut a,b| {
                a.extend(b);
                a
            });
        Ok(FaiMatrix { values, len: traces.traces_len() })
    } 


    pub fn len(&self) -> usize {
        self.len
    }

    pub fn save_npy_file<P: AsRef<Path>>(&self, path: P) -> anyhow::Result<()> {
        let mut array: Vec<Vec<f32>> = vec![vec![1.0f32; self.len]; self.len];

        for i in 0..self.len {
            for j in i+1..self.len {
                if let Some(v) = self.values.get(&(i, j)) {
                    array[i][j] = *v;
                }
                
            }
        }

        let mut out_buf = vec![];
        let mut writer = {
            npyz::WriteOptions::new()
                .default_dtype()
                .shape(&[self.len as u64, self.len as u64])
                .writer(&mut out_buf)
                .begin_nd()?
        };
        for row in 0..self.len {
            writer.extend(&array[row])?;
        }

        writer.finish()?;

        let mut file = File::create(path)?; // Open the file in write mode
        file.write_all(&out_buf)?;

        Ok(())

    }

    // compute all fai for this index, save to npy file
    pub fn deep_analysis<P: AsRef<Path>>(path: P, traces: &TraceMatrixColumns, estimator: &EntropyEstimator, t0: usize) -> anyhow::Result<()> {
        let i1range: Vec<usize> = (0..(traces.traces_len())).collect(); 
        println!("irange length: {}", i1range.len());
        let mut map_results: Vec<f32> = i1range.par_iter().map(|pi1| {
            let i1 = *pi1;
        
            let fai = fractionnal_added_information_1to1(traces.column(i1), traces.column(t0), estimator).unwrap();
            return fai;
        }).collect();

        map_results[t0] = 1.0f32;

        let mut out_buf = vec![];
        let mut writer = {
            npyz::WriteOptions::new()
                .default_dtype()
                .shape(&[traces.traces_len() as u64])
                .writer(&mut out_buf)
                .begin_nd()?
        };

        writer.extend(&map_results)?;
        writer.finish()?;

        let mut file = File::create(path)?; // Open the file in write mode
        file.write_all(&out_buf)?;
        

        Ok(())
    }

    pub fn minmax(traces: &TraceMatrixColumns) -> anyhow::Result<()> {
        let min = traces.min();
        let max = traces.max();

        let mut out_buf = vec![];
        let mut writer = {
            npyz::WriteOptions::new()
                .default_dtype()
                .shape(&[2, traces.traces_len() as u64])
                .writer(&mut out_buf)
                .begin_nd()?
        };

        writer.extend(&min)?;
        writer.extend(&max)?;
        writer.finish()?;

        let mut file = File::create("minmax.npy")?; // Open the file in write mode
        file.write_all(&out_buf)?;
        

        Ok(())
    }

    pub fn deep_analysis2<P: AsRef<Path>>(path: P, traces: &TraceMatrixColumns, estimator: &EntropyEstimator, t0: usize, t1:usize) -> anyhow::Result<()> {
        let i1range: Vec<usize> = (0..(traces.traces_len())).collect(); 
        println!("irange length: {}", i1range.len());
        let mut map_results: Vec<f32> = i1range.par_iter().map(|pi1| {
            let i1 = *pi1;
        
            let fai = fractionnal_added_information_2to1(traces.column(i1), traces.column(t0), traces.column(t1), estimator).unwrap();
            return fai;
        }).collect();

        map_results[t0] = 1.0f32;
        map_results[t1] = 1.0f32;

        let mut out_buf = vec![];
        let mut writer = {
            npyz::WriteOptions::new()
                .default_dtype()
                .shape(&[traces.traces_len() as u64])
                .writer(&mut out_buf)
                .begin_nd()?
        };

        writer.extend(&map_results)?;
        writer.finish()?;

        let mut file = File::create(path)?; // Open the file in write mode
        file.write_all(&out_buf)?;
        

        Ok(())
    }
}

impl Index<(usize, usize)> for FaiMatrix {
    type Output = f32;

    fn index(&self, indexes: (usize, usize)) -> &Self::Output {
        self.values.get(&indexes).unwrap_or(&1f32)
    }
}

impl FileStorable for FaiMatrix {}


pub struct TwoSourcesAnalyzer {
    // i1: usize,
    // i2: usize,
    pub noise_threshold: f32,
    // window: usize,
    // fais_under_threshold: Vec<(usize, f32)>,
    pub identified_2to1: Vec<(usize, f32)>
}

impl TwoSourcesAnalyzer {

    fn fai_from_2_sources_under_threshold(traces: &TraceMatrixColumns, i1: usize, i2: usize, estimator: &EntropyEstimator, noise_threshold: f32, mut window: usize) -> Vec<(usize, f32)> {
        if window == 0 {
            window = traces.traces_len();
        }
    
        let threshold = 1f32 - noise_threshold;
    
        let vec1 = traces.column(i1);
        let vec2 = traces.column(i2);
    
        let max_ind: usize = min(i2 + window, traces.traces_len());
        let start_index: usize = max(i1, i2) + 1;
        let range_to_explore: Vec<usize> = ((start_index..max_ind)).collect();

        let knowing_entropy: f32 = shannon_entropy2(&vec1, &vec2, &estimator).unwrap();
    
        let fais: Vec<(usize, f32)> = range_to_explore.into_iter().map(|i| (i, fractionnal_added_information_2to1_memoize_knowing(traces.column(i), vec1, vec2, knowing_entropy, estimator).unwrap())).collect();
        return fais.into_iter().filter(|(_, w)| *w < threshold).collect();
    }

    // hg contains known relations
    pub fn new(traces: &TraceMatrixColumns, i1: usize, i2: usize, hg: &HGraph, estimator: &EntropyEstimator, noise_threshold: f32, window: usize) -> TwoSourcesAnalyzer {
        let fais_under_threshold = TwoSourcesAnalyzer::fai_from_2_sources_under_threshold(traces, i1, i2, estimator, noise_threshold, window);
        // let threshold = 1f32 - noise_threshold;

        // now we check if detected points of interest have alternaive information flow in hg

        // 1. is there a 1 to 1 edge with similar information content, from only one of the inputs
        let identified: Vec<(usize, f32)> = fais_under_threshold.iter().filter(|(i3, w2to1)| {
            let h1 = HEdge::new1to1(i1, *i3);
            let h2 = HEdge::new1to1(i2, *i3);
            let w1 = hg.edge_weight(&h1);
            let w2 = hg.edge_weight(&h2);

            let w1to1 = if w1 < w2 { w1 } else { w2 }; // min, we know there are no NaN or Inf

            return *w2to1 < (w1to1 - noise_threshold);
        }).map(|(pi,pw)| (*pi, *pw)).collect();

        
        let identified_2to1 = /*simplified*/ identified;
        // TwoSourcesAnalyzer { i1, i2, noise_threshold, window, fais_under_threshold, identified_2to1 }
        TwoSourcesAnalyzer { noise_threshold, identified_2to1 }
    }

    // a try at detecting transive edges
    // should be better done by ancestor analysis in graph
    pub fn simplified(&self, hg: &HGraph) -> Vec<(usize, f32)> {
        let threshold = 1f32 - self.noise_threshold;
        // 2. Remove transitive edges: is the 2to1 relation "(i1, i2) => i3" in fact a transitive relation "(i1, i2) => i => i3"
        // we suppose [identified] sorted by trace index
        let simplified = self.identified_2to1.iter().filter(|(i3, w2_i3)| {
            for (i, w2_i) in self.identified_2to1.iter() {
                let w_i_i3 = hg.edge_weight(&HEdge::new1to1(*i, *i3));
                if w_i_i3 < threshold && (threshold - (1f32 - *w2_i)*(1f32 - w_i_i3)) < *w2_i3 { // heuristic
                    return false;
                }
            }
            true
        }).map(|(pi, pw)| (*pi, *pw));

        return simplified.collect();
    }

}



#[cfg(test)]
mod tests {
    
}