import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams.update({'font.size': 20})

fig, ax = plt.subplots()



# filenames = ["u2", "u8", "u64", "u128", "u256", "hamming"]
filenames = ["n2", "n8", "n256", "hamming"]
labels = ["Uniform 2", "Uniform 8", "Uniform 256", "Hamming weight"]

for i, filename in enumerate(filenames):
    
    unique_values, counts = np.unique(np.load(filename + ".npy"), return_counts=True)
    s = np.sum(counts)
    counts = counts / s
    cumulative_probabilities = np.cumsum(counts)
    ax.plot(unique_values, cumulative_probabilities, label=labels[i])
    maxuv = np.max(unique_values)
    print(labels[i], "max observations:", maxuv)

plt.xlabel("Number of observations")
plt.ylabel("Cumulative probability of being able to distinguish 256 vectors")
plt.legend()
plt.show()