use rand;
use rand::distributions::Uniform;
use rand::distributions::Distribution;
use rand::Rng;

use npyz::WriterBuilder;
use std::fs::File;
use std::path::Path;
use std::io::Write;
use std::convert::Into;

use rayon::prelude::*;

use indicatif::{ParallelProgressIterator};

trait Sampler {
    fn sample(&self, value: u8) -> u8;
    fn generate_random_vector(&mut self, vector_size: usize) -> Vec<u8>;
}

pub struct UniformSampler {
    // samples_count: usize,
    sampling_table: [u8; 256],
    uniform_bytes: Uniform<u8>,
    rng:  rand::rngs::ThreadRng,
}

impl UniformSampler {
    pub fn new(samples_count: usize) -> UniformSampler {
        let mut sampling_table: [u8; 256] = [0u8; 256];
        let rng = rand::thread_rng();

        for i in 0..256 {
            sampling_table[i] = (i % samples_count) as u8;
        }

        let uniform_bytes = Uniform::new_inclusive(0u8, 255u8);

        UniformSampler {
            // samples_count,
            sampling_table,
            uniform_bytes,
            rng
        }
    }
}


impl Sampler for UniformSampler {

    fn sample(&self, value: u8) -> u8 {
        return self.sampling_table[value as usize];
    }

    fn generate_random_vector(&mut self, vector_size: usize) -> Vec<u8> {
        // Generate a vector of random u8 values
        let random_u8_vector: Vec<u8> = self.uniform_bytes.sample_iter(&mut self.rng).take(vector_size).collect();
        return random_u8_vector.into_iter().map(|v|self.sample(v)).collect();
    }
}

pub struct HammingSampler {
    uniform_bytes: Uniform<u8>,
    rng:  rand::rngs::ThreadRng,
}

impl HammingSampler {
    pub fn new() -> HammingSampler {
        let rng = rand::thread_rng();
        let uniform_bytes = Uniform::new_inclusive(0u8, 255u8);

        HammingSampler {
            rng,
            uniform_bytes
        }
    }
}

impl Sampler for HammingSampler {

    fn sample(&self, value: u8) -> u8 {
        return value.count_ones() as u8;
    }

    fn generate_random_vector(&mut self, vector_size: usize) -> Vec<u8> {
        // Generate a vector of random u8 values
        let random_u8_vector: Vec<u8> = self.uniform_bytes.sample_iter(&mut self.rng).take(vector_size).collect();
        return random_u8_vector.into_iter().map(|v|self.sample(v)).collect();
    }
}

#[derive(Debug,Clone,Copy)]
pub enum DontCareU8 {
    Care(u8),
    DontCare
}

impl PartialEq for DontCareU8 {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            // If both are DontCare, they are considered equal
            (DontCareU8::DontCare, DontCareU8::DontCare) => true,
            // If one of them is DontCare, they are considered equal
            (DontCareU8::DontCare, DontCareU8::Care(_)) => true,
            (DontCareU8::Care(_), DontCareU8::DontCare) => true,
            // If both are Care, then compare the embedded u8 values
            (DontCareU8::Care(a), DontCareU8::Care(b)) => a == b,
        }
    }
}



fn are_vec_eq<T: PartialEq>(v1: &Vec<T>, v2: &Vec<T>) -> bool {
    if v1.len() != v2.len() {
        return false;
    }
    else {
        for i in 0..v1.len() {
            if v1[i] != v2[i] {
                return false;
            }
        }
        return true;
    }
}

// return count of observation before first difference detected
fn observe_and_distinguish2<S: Sampler>(vector_size: usize, sampler: &mut S) -> usize {
    let v1 = sampler.generate_random_vector(vector_size);
    let v2 = sampler.generate_random_vector(vector_size);

    if are_vec_eq(&v1, &v2) {
        return 0;
    }

    let mut obs_v1 = vec![DontCareU8::DontCare; vector_size];
    let mut obs_v2 = vec![DontCareU8::DontCare; vector_size];

    let mut rng = rand::thread_rng();
    let dist = Uniform::new(0usize, vector_size);

    const MAX_OBSERVATIONS: usize = 1000;
    for o in 1..=MAX_OBSERVATIONS {
        // add 1 observation for each vec
        let index_obs_v1 = rng.sample(dist);
        let index_obs_v2 = rng.sample(dist);

        obs_v1[index_obs_v1] = DontCareU8::Care(v1[index_obs_v1]);
        obs_v2[index_obs_v2] = DontCareU8::Care(v2[index_obs_v2]);

        if !are_vec_eq(&obs_v1, &obs_v2) {
            return o;
        }
    }

    return MAX_OBSERVATIONS+1;
}

fn observe_and_distinguish_n_vectors<S: Sampler>(vector_size: usize, vectors_count: usize, sampler: &mut S) -> usize {
    // generate [vectors_count] different vectors
    let mut vectors: Vec<Vec<u8>> = Vec::with_capacity(vectors_count);

    for _ in 0..vectors_count {
        loop {
            let new_v = sampler.generate_random_vector(vector_size);

            // must be different from all other
            let mut has_same = false;
            for other_v in vectors.iter() {
                if other_v == &new_v {
                    has_same = true;
                }
            }

            if !has_same {
                vectors.push(new_v);
                break;
            }
        }
    }

    let mut obs_vectors: Vec<Vec<DontCareU8>> = vec![vec![DontCareU8::DontCare; vector_size]; vectors_count];

    // rng and distribution to choose observation index
    let mut rng = rand::thread_rng();
    let dist = Uniform::new(0usize, vector_size);

    const MAX_OBSERVATIONS: usize = 1000;
    for o in 1..=MAX_OBSERVATIONS {
        // add 1 observation for each vec
        for i in 0..vectors_count {
            let index_obs = rng.sample(dist);
            obs_vectors[i][index_obs] = DontCareU8::Care(vectors[i][index_obs]);
        }

        // can we distinguish all vectors

        let mut has_equal = false;
        for i1 in 0..(vectors_count - 1) {
            for i2 in (i1+1)..vectors_count {
                if are_vec_eq(&obs_vectors[i1], &obs_vectors[i2]) {
                    has_equal = true;
                    break;
                }
            }

            if has_equal {
                break;
            }
        }

        if !has_equal {
            return o;
        }   
    }

    return MAX_OBSERVATIONS+1;
}

pub fn save_vec_to_npy<P: AsRef<Path>, T: Into<u32>>(vec: Vec<T>, path: P) -> anyhow::Result<()> {

    let vec32: Vec<u32> = vec.into_iter().map(|v| v.into()).collect();
    let mut out_buf = vec![];
    let mut writer = {
        npyz::WriteOptions::new()
            .default_dtype()
            .shape(&[vec32.len() as u64])
            .writer(&mut out_buf)
            .begin_nd()?
    };
    writer.extend(vec32)?;
    writer.finish()?;

    let mut file = File::create(path)?; // Open the file in write mode
    file.write_all(&out_buf)?;

    Ok(())

}

fn evaluate_uniform_sampler2(samples_count: usize) -> anyhow::Result<()> {
    let mut sampler = UniformSampler::new(samples_count);
   
    let results: Vec<u32> = (0..100000).into_iter().map(|_|{
        observe_and_distinguish2(257, &mut sampler) as u32
    }).collect();
    let path = format!("u{}.npy", samples_count);
    save_vec_to_npy(results, path)?;
    Ok(())
}

fn evaluate_uniform_sampler_n(samples_count: usize) -> anyhow::Result<()> {
    let mut sampler = UniformSampler::new(samples_count);
   
    let results: Vec<u32> = (0..100000).into_iter().map(|_|{
        observe_and_distinguish_n_vectors(257, 256, &mut sampler) as u32
    }).collect();
    let path = format!("n{}.npy", samples_count);
    save_vec_to_npy(results, path)?;
    Ok(())
}

fn evaluate_hamming_sampler2() -> anyhow::Result<()> {
    let mut sampler = HammingSampler::new();
   
    let results: Vec<u32> = (0..100000).into_iter().map(|_|{
        observe_and_distinguish2(257, &mut sampler) as u32
    }).collect();
    let path = format!("hamming.npy");
    save_vec_to_npy(results, path)?;
    Ok(())
}

fn evaluate_hamming_sampler_n() -> anyhow::Result<()> {
    let mut sampler = HammingSampler::new();
   
    let results: Vec<u32> = (0..100000).into_iter().map(|_|{
        observe_and_distinguish_n_vectors(257, 256, &mut sampler) as u32
    }).collect();
    let path = format!("hamming.npy");
    save_vec_to_npy(results, path)?;
    Ok(())
}

fn main() -> anyhow::Result<()> {
    println!("Starting...");

    let range = vec![2, 4, 8, 9, 16, 32, 64, 128, 256];
    let range_len = range.len() as u64;
    let _vec: Vec<_> = range.into_par_iter().progress_count(range_len).map(|s|{
        // let mut samp = UniformSampler::new(s);
        // let v = samp.generate_random_vector(100000);
        // let _ = save_vec_to_npy(v, format!("v{}.npy", s));

        match evaluate_uniform_sampler_n(s) {
            Err(e) => {println!("Error: {}", e);}
            _ => {}
        }
    }).collect();

    evaluate_hamming_sampler_n()?;

    println!("The end.");

    Ok(())
}
