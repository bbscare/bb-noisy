use bbscare_libs::information::entropies::{EntropyEstimator};
use bbscare_libs::information::flow_analysis::{FaiMatrix};
use bbscare_libs::information::hgraph::{HGraph/*, HEdgeInput*/};
use bbscare_libs::loaders::trace_matrix::{TraceMatrixColumns, TraceMatrixLines, SimpleFilteringResults, TraceMatrixLinesBatchReader, TraceMatrixLinesJointBatchReader};
// use bbscare_libs::information::hgraph::HEdgeInput;
use bbscare_libs::loaders::file_storable::FileStorable;
use bbscare_libs::information::clusters::Clusters;
use bbscare_libs::information::clusters::perfect_raw_to_logic_traces;
use bbscare_libs::information::functions::Functions;
use bbscare_libs::information::io_functions::IOFunctions;
use bbscare_libs::information::predictor::Predictor;
use bbscare_libs::lossy::smart_enrich;

use std::env;
use std::path::Path;
use std::time::{Instant};
use std::io::{Write};
use std::fmt::Write as FmtWrite;

use nalgebra::DMatrix;

use rayon::iter::ParallelIterator;
use rayon::iter::IntoParallelRefIterator;


use rayon;
use num_cpus;

const TRACES_COUNT: usize = 1000000;
const WINDOW: usize = 256;
const NOISE_THRESHOLD: f32 = 0.05;
const BATCH_SIZE: usize = 10000;

// data files
const ESTIMATORS_FILENAME: &str = "entropy_estimators";
const RAW_TRACES_FILENAME: &str = "traces_processed_1M.binu8";
const POOR_LOGIC_TRACES_FILENAME: &str = "poor_traces_logic.binu8";
const RICH_LOGIC_TRACES_FILENAME: &str = "rich_traces_logic.binu8";
const LOGIC_TRACES_COLUMNS_FILENAME: &str = "traces_logic_columns.binu8";
const RAW_TRACES_IF_FILENAME: &str = "raw_traces_if.binu8";
const PTS_FILENAME: &str = "pts_1M.binu8";
const PTS_COLUMNS_FILENAME: &str = "io_columns.in";
const CTS_FILENAME: &str = "cts_1M.binu8";
const CTS_COLUMNS_FILENAME: &str = "io_columns.out";

// analysis files
const FAI_MATRIX_FILENAME: &str = "fai_matrix";
const RAW_HG_FILENAME: &str = "hg";
const LOGIC_HG_FILENAME: &str = "logic_hg";
const CLUSTERS_FILENAME: &str = "clusters";
const FILTERING_FILENAME: &str = "filtering";
const FUNCTIONS_FILENAME: &str = "functions";
const IO_FUNCTIONS_FILENAME: &str = "io_functions";

// fn change_word_endian(v: &Vec<u8>) -> Vec<u8> {
//     let length = v.len();
    
//     if length % 4 != 0 {
//         panic!("Only vectors with a length a multiple of 4 are accepted.");
//     }

//     let mut new_v: Vec<u8> = Vec::with_capacity(length);
//     for word_index in 0..(length >> 2) {
//         for byte_index in (0..4).into_iter().rev() {
//             new_v.push(v[word_index*4 + byte_index]);
//         }
//     }
//     new_v
// }

fn vec2str(v: &Vec<u8>) -> String {
    let mut s: String = String::new();

    for b in v {
        write!(&mut s, "{:02x}", b).expect("vec2str error");
    }

    s
}

// Load or compute
fn load_estimator<P: AsRef<Path>>(path: P) -> anyhow::Result<EntropyEstimator> {
    print!("Loading estimators...");
    let start = Instant::now();
    std::io::stdout().flush()?;
    let mut estimator = match EntropyEstimator::from_file(&path) {
        Ok(e) => e,
        Err(_) => {// probably file not found
            EntropyEstimator::new()
         }
    };

    // make sure we have the required estimators
    let mut added_count = 0;
    for d in 1..4 {
        if estimator.add_estimator(TRACES_COUNT,d) { added_count += 1 }; // add only if non-existent
    }
    if added_count > 0 { // save if any change
        estimator.save_to_file(path)?;
    }

    let duration = start.elapsed();
    println!("Done ({:?} seconds).",duration.as_secs());
    
    return Ok(estimator);
}

// Load or compute
fn load_raw_traces_if<P: AsRef<Path>>(path: P) -> anyhow::Result<TraceMatrixColumns> {
    match TraceMatrixColumns::from_file(path.as_ref()) {
        Ok(tmc) => Ok(tmc),
        Err(_) => { // traces not found, we must generate them
            print!("{} not found, generating...", path.as_ref().display());
            std::io::stdout().flush()?;
            let start = Instant::now();
            // get raw traces, fail if not present
            let raw_traces = TraceMatrixLines::from_file_truncated(get_data_file(RAW_TRACES_FILENAME), TRACES_COUNT)?;
            let mut raw_traces_columns = TraceMatrixColumns::from_lines(&raw_traces);
            // print!("{} columns found in raw file.", raw_traces_columns.traces_len());

            //get pts and cts
            // we do that here to trigger recomputation if we delete raw_traces_if.binu8
            let pts = TraceMatrixLines::from_file_truncated(get_data_file(PTS_FILENAME), TRACES_COUNT)?;
            let pts_columns = TraceMatrixColumns::from_lines(&pts);
            pts_columns.save_to_file(get_data_file(PTS_COLUMNS_FILENAME))?;

            let cts = TraceMatrixLines::from_file_truncated(get_data_file(CTS_FILENAME), TRACES_COUNT)?;
            let cts_columns = TraceMatrixColumns::from_lines(&cts);
            cts_columns.save_to_file(get_data_file(CTS_COLUMNS_FILENAME))?;


            // append [pts raw_traces cts]
            raw_traces_columns.append_prefix(&pts_columns);
            raw_traces_columns.append_postfix(&cts_columns);

            // filter resulting traces
            let filtering = raw_traces_columns.simple_filtering();
            filtering.save_to_file(get_data_file(FILTERING_FILENAME))?;

            let duration = start.elapsed();
            println!("Done ({:?} seconds).",duration.as_secs());

            println!("{} columns found in appended raw traces.", filtering.columns_before_filtering());
            println!("{} columns after removing constants.", filtering.columns_constants_removed());
            println!("{} columns after removing identical ones.", filtering.columns_constants_identical_removed());
    
            raw_traces_columns.save_to_file(path.as_ref())?;
            Ok(raw_traces_columns)
        }
    }
}

// load or compute
fn load_fai_matrix<P: AsRef<Path>>(path: P, raw_traces_if: &TraceMatrixColumns, estimator: &EntropyEstimator) -> anyhow::Result<FaiMatrix> {
    match FaiMatrix::from_file(path.as_ref()) {
        Ok(f) => Ok(f),
        Err(_) => { // file not found or format error
            print!("Computing FAI matrix...");
            std::io::stdout().flush()?;
            let start = Instant::now();
            let f = FaiMatrix::from_traces(raw_traces_if, estimator, WINDOW)?;
            let duration = start.elapsed();
            println!("Done ({:?} seconds).",duration.as_secs());
            f.save_to_file(path.as_ref())?;
            f.save_npy_file(get_data_file("fai.npy"))?;
            Ok(f)
        }
    }
}

// Load or compute
fn load_raw_hg<P: AsRef<Path>>(path: P, traces: &TraceMatrixColumns, estimator: &EntropyEstimator, fai_matrix: &FaiMatrix) -> anyhow::Result<HGraph> {
    match HGraph::from_file(path.as_ref()) {
        Ok(f) => Ok(f),
        Err(_) => { //file not found or format error
            print!("Computing hgraph...");
            std::io::stdout().flush()?;
            let start = Instant::now();
            let mut hg = HGraph::from_fai_matrix(fai_matrix, NOISE_THRESHOLD);
            let duration = start.elapsed();
            println!("1-to-1 done ({:?} seconds).",duration.as_secs());
            hg.search_for_2to1(traces, estimator, NOISE_THRESHOLD, WINDOW)?;
            hg.progressive_simplification(NOISE_THRESHOLD);
            hg.save_to_file(path.as_ref())?;
            Ok(hg)
        }
    }
}

fn load_logic_hg<P: AsRef<Path>>(path: P, raw_hg: &HGraph, clusters: &Clusters, logic_traces_if: &TraceMatrixColumns, estimator: &EntropyEstimator) -> anyhow::Result<HGraph> {
    match HGraph::from_file(path.as_ref()) {
        Ok(f) => Ok(f),
        Err(_) => { //file not found or format error
            print!("Computing rich hgraph...");
            std::io::stdout().flush()?;
            let start = Instant::now();
            let mut logic_hg = raw_hg.to_logic_hgraph(clusters);
            logic_hg.ensure_timing_causality();
            logic_hg.update_weight(logic_traces_if, estimator);
            logic_hg.progressive_simplification(NOISE_THRESHOLD);
            let duration = start.elapsed();
            println!("done ({:?} seconds).",duration.as_secs());
            
            logic_hg.save_to_file(path.as_ref())?;
            Ok(logic_hg)
        }
    }
}

// Load or compute
fn load_clusters<P: AsRef<Path>>(path: P, hg: &HGraph, raw_traces_if: &TraceMatrixColumns, estimator: &EntropyEstimator) -> anyhow::Result<Clusters> {
    match Clusters::from_file(path.as_ref()) {
        Ok(f) => Ok(f),
        Err(_) => { //file not found or format error
            print!("Computing clusters...");
            std::io::stdout().flush()?;
            let start = Instant::now();
            let clusters = Clusters::from_hgraph(hg, raw_traces_if, estimator)?;
            let duration = start.elapsed();
            println!("done ({:?} seconds).",duration.as_secs());
            clusters.save_to_file(path.as_ref())?;
            Ok(clusters)
        }
    }
}

// Load or compute
fn load_functions<P: AsRef<Path>>(path: P, logic_hg: &HGraph, logic_traces: &mut TraceMatrixLinesBatchReader) -> anyhow::Result<Functions> {
    match Functions::from_file(path.as_ref()) {
        Ok(f) => Ok(f),
        Err(_) => { //file not found or format error
            print!("Computing functions...");
            std::io::stdout().flush()?;
            let start = Instant::now();
            let functions = Functions::learn(logic_hg, logic_traces)?;
            let duration = start.elapsed();
            println!("done ({:?} seconds).",duration.as_secs());
            functions.save_to_file(path.as_ref())?;
            Ok(functions)
        }
    }
}

// Load or compute
fn load_io_functions<P: AsRef<Path>>(path: P, joint_traces: &mut TraceMatrixLinesJointBatchReader, inputs_mappings: &Vec<usize>, outputs_mappings: &Vec<usize>) -> anyhow::Result<IOFunctions> {
    match IOFunctions::from_file(path.as_ref()) {
        Ok(f) => Ok(f),
        Err(_) => { //file not found or format error
            print!("Computing io functions...");
            std::io::stdout().flush()?;
            let start = Instant::now();
            let functions = IOFunctions::learn(joint_traces, inputs_mappings, outputs_mappings)?;
            let duration = start.elapsed();
            println!("done ({:?} seconds).",duration.as_secs());
            functions.save_to_file(path.as_ref())?;
            Ok(functions)
        }
    }
}

fn compute_logic_traces<P: AsRef<Path>>(path: P, filtering: &SimpleFilteringResults, clusters: &mut Clusters) -> anyhow::Result<()> {
    if !path.as_ref().exists() {
        perfect_raw_to_logic_traces(get_data_file(RAW_TRACES_FILENAME), get_data_file(PTS_FILENAME), get_data_file(CTS_FILENAME), path, filtering, clusters)?;
    }
    Ok(())
}

fn load_logic_traces_columns<P1: AsRef<Path>, P2: AsRef<Path>>(path: P1, lines: P2) -> anyhow::Result<TraceMatrixColumns> {
    if !path.as_ref().exists() {
        print!("Computing logic traces columns...");
        std::io::stdout().flush()?;
        let start = Instant::now();
        let lines = TraceMatrixLines::from_file(lines.as_ref())?;
        let columns = TraceMatrixColumns::from_lines(&lines);
        let duration = start.elapsed();
        println!("done ({:?} seconds).",duration.as_secs());
        columns.save_to_file(path.as_ref())?;
        Ok(columns)
    }
    else {
        let columns = TraceMatrixColumns::from_file(path.as_ref())?;
        Ok(columns)
    }
}

fn compute_rich_logic_traces<P1: AsRef<Path>>(path: P1, mut logic_traces_columns: TraceMatrixColumns, logic_hg: &HGraph, enriched: &mut Vec<bool>) -> anyhow::Result<()> {
    if !path.as_ref().exists() {
        print!("Computing rich logic traces...");
        std::io::stdout().flush()?;
        let start = Instant::now();
        
        
        smart_enrich(&mut logic_traces_columns, logic_hg, enriched)?;
        let rich_logic_traces = TraceMatrixLines::from_columns(&logic_traces_columns);        

        let duration = start.elapsed();
        println!("done ({:?} seconds).",duration.as_secs());
        rich_logic_traces.write_to_file(path.as_ref())?;
        Ok(())
    }
    else {
        Ok(())
    }
}

fn covariance() -> anyhow::Result<()> {

    print!("Computing covariance matrix...");
    std::io::stdout().flush()?;
    let start = Instant::now();

    let raw_traces = TraceMatrixLines::from_file_truncated(get_data_file(RAW_TRACES_FILENAME), TRACES_COUNT)?;
    let traces_len = raw_traces.traces_len();
    let dm = DMatrix::<f32>::zeros(traces_len, traces_len);

    // compute mean trace
    let mut mean_traces: Vec<f32> = vec![0f32; traces_len];
    for row in raw_traces.lines.iter() {
        for (i, r) in row.iter().enumerate() {
            mean_traces[i] += *r as f32;
        }
    }   



    raw_traces.lines.par_iter().map(|r| {

    });

    let duration = start.elapsed();
    println!("done ({:?} seconds).",duration.as_secs());
    Ok(())
}

fn get_data_file(filename: &str) -> String {
    let data_folder: String = match env::var("DATA") {
        Ok(d) => d,
        Err(_) => "../../data".to_owned()
    };
    return data_folder + "/" + filename;
}

fn main() -> anyhow::Result<()> {
    rayon::ThreadPoolBuilder::new().num_threads(num_cpus::get_physical()).build_global().unwrap();
    // get or compute entropy estimators
    let estimator = load_estimator(get_data_file(ESTIMATORS_FILENAME))?;
    
    // get raw traces
    println!("Getting raw traces for information flow analysis.");
    let raw_traces_if = load_raw_traces_if(get_data_file(RAW_TRACES_IF_FILENAME))?;

    assert!(Path::new(&get_data_file(PTS_COLUMNS_FILENAME)).exists(), "{} file not found.", PTS_COLUMNS_FILENAME);
    assert!(Path::new(&get_data_file(CTS_COLUMNS_FILENAME)).exists(), "{} file not found.", CTS_COLUMNS_FILENAME);
    assert!(Path::new(&get_data_file(FILTERING_FILENAME)).exists(), "{} file not found.", FILTERING_FILENAME);

    let filtering = SimpleFilteringResults::from_file(get_data_file(FILTERING_FILENAME))?;

    // start flow analysis
    println!("Starting information flow analysis.");
    let fai_matrix = load_fai_matrix(get_data_file(FAI_MATRIX_FILENAME), &raw_traces_if, &estimator)?;


    FaiMatrix::deep_analysis("v00.npy", &raw_traces_if, &estimator, 0)?;
    FaiMatrix::deep_analysis("v01.npy", &raw_traces_if, &estimator, 1)?;
    FaiMatrix::deep_analysis("v02.npy", &raw_traces_if, &estimator, 2)?;
    FaiMatrix::deep_analysis("v03.npy", &raw_traces_if, &estimator, 3)?;
    // FaiMatrix::deep_analysis("v10.npy", &raw_traces_if, &estimator, 10)?;    
    FaiMatrix::deep_analysis2("v0001.npy",&raw_traces_if, &estimator, 0,1)?;
    FaiMatrix::deep_analysis2("v0002.npy",&raw_traces_if, &estimator, 0,2)?;
    FaiMatrix::deep_analysis2("v0003.npy",&raw_traces_if, &estimator, 0,3)?;
    // FaiMatrix::deep_analysis2("v0010.npy",&raw_traces_if, &estimator, 0,10)?;
    // FaiMatrix::deep_analysis2("v0506.npy",&raw_traces_if, &estimator, 5,6)?;
    // FaiMatrix::deep_analysis2("vrand.npy",&raw_traces_if, &estimator, 100,200)?;

    // FaiMatrix::minmax(&raw_traces_if)?;


    // covariance()?;
    // let hg = load_raw_hg(get_data_file(RAW_HG_FILENAME), &raw_traces_if, &estimator, &fai_matrix)?;

    // let (c1to1, c2to1) = hg.edge_types_count();
    // println!("Raw hg: {} edges in hgraph. {} 1-to-1, {} 2-to-1.", hg.edges_count(), c1to1, c2to1);

    // for t in [/*87, 110, 223, */224] {
    //     // println!("{}: {:?}", t, hg.edges_ending_in(t));
    //     println!("{}:", t);
    //     // HGraph::pretty_print_edges(&logic_hg.edges_ending_in(t));
    //     HGraph::pretty_print_edges(&hg.edges_ending_in(t));
    //     let debug = hg.minimum_ancestor_sets(vec![t]);
    //     println!("Min {}: {:?}", t, debug);
    // }

    // let mut clusters = load_clusters(get_data_file(CLUSTERS_FILENAME), &hg, &raw_traces_if, &estimator)?;
    // println!("Clustering done");

    // compute_logic_traces(get_data_file(POOR_LOGIC_TRACES_FILENAME), &filtering, &mut clusters)?;
   
    // let poor_logic_traces_lines = TraceMatrixLines::from_file_truncated(get_data_file(POOR_LOGIC_TRACES_FILENAME), TRACES_COUNT)?;
    // let poor_logic_traces_columns = TraceMatrixColumns::from_lines(&poor_logic_traces_lines);

    //  // finding where inputs and outputs can be found in logic traces
    // let inputs = TraceMatrixColumns::from_file(get_data_file(PTS_COLUMNS_FILENAME))?;
    // let pts_ind = poor_logic_traces_columns.find_inputs(&inputs, NOISE_THRESHOLD, &estimator)?;

    // let outputs = TraceMatrixColumns::from_file(get_data_file(CTS_COLUMNS_FILENAME))?;
    // let cts_ind = poor_logic_traces_columns.find_outputs(&outputs, NOISE_THRESHOLD, &estimator)?;

    // println!("pts_ind: {:?}", pts_ind);
    // println!("cts_ind: {:?}", cts_ind);

    // let logic_hg = load_logic_hg(get_data_file(LOGIC_HG_FILENAME), &hg, &clusters, &poor_logic_traces_columns, &estimator)?;
    // let (c1to1, c2to1) = logic_hg.edge_types_count();
    // println!("Logic hg: {} edges in hgraph. {} 1-to-1, {} 2-to-1.", logic_hg.edges_count(), c1to1, c2to1);

    // for t in 50..150 {

    //     let ending_in = logic_hg.edges_ending_in(t);
    //     let has1to1 = {
    //         let mut has = false;
    //         for (e, _) in ending_in.iter() {
    //             match e.inputs {
    //                 HEdgeInput::One(_) => { has = true; },
    //                 _ => {}
    //             }
    //         }
    //         has
    //     };
    //     if has1to1 {
    //         println!("{}:", t);
    //         HGraph::pretty_print_edges(&logic_hg.edges_ending_in(t));
    //         for (e, _) in logic_hg.edges_ending_in(t) {
    //             match e.inputs {
    //                 HEdgeInput::One(i1) => {
    //                     println!("{:?} ({:?})", clusters.find_raws_from_logic(i1), clusters.cluster_key(i1).unwrap());
    //                 },
    //                 _ => {}
    //             }
    //         }
    //         println!("\t-> {:?} ({:?})", clusters.find_raws_from_logic(t), clusters.cluster_key(t).unwrap());
    //     }
    // }

    // // // let vs = vec![7, 23, 38, 52, 53, 55, 56, 147, 150, 152];
    // let vs = vec![101, 208, 227, 372, 374];
    // for v in vs {
    //     println!("Min ancestors for {}: {:?}", v, hg.minimum_ancestor_sets(vec![v]));
    // }


    // use bbscare_libs::information::entropies::fractionnal_added_information_2to1;
    // let fai149_1 = fractionnal_added_information_2to1(raw_traces_if.column(227), raw_traces_if.column(5), raw_traces_if.column(96), &estimator)?;
    // let fai149_7 = fractionnal_added_information_2to1(raw_traces_if.column(227), raw_traces_if.column(0), raw_traces_if.column(101), &estimator)?;

    // println!("Fai 227. (5, 96): {}, (0, 101): {}", fai149_1, fai149_7);

    // return Ok(());

    // let poor_logic_traces = TraceMatrixLines::from_file(get_data_file(POOR_LOGIC_TRACES_FILENAME))?;
    // println!("Poor logic traces length {}", poor_logic_traces.traces_len());

    // let mut enriched: Vec<bool> = vec![false; poor_logic_traces.traces_len()];
    // for i in pts_ind.iter() {
    //     enriched[*i] = true;
    // }
    // for i in cts_ind.iter() {
    //     enriched[*i] = true;
    // }

    // let logic_traces_columns = load_logic_traces_columns(get_data_file(LOGIC_TRACES_COLUMNS_FILENAME), get_data_file(POOR_LOGIC_TRACES_FILENAME))?;
    // compute_rich_logic_traces(get_data_file(RICH_LOGIC_TRACES_FILENAME), logic_traces_columns, &logic_hg, &mut enriched)?;    
   

    // let mut rich_logic_traces = TraceMatrixLinesBatchReader::new(get_data_file(RICH_LOGIC_TRACES_FILENAME), BATCH_SIZE)?;
    // let functions = load_functions(get_data_file(FUNCTIONS_FILENAME), &logic_hg, &mut rich_logic_traces)?;

    // let mut joint_logic_traces = TraceMatrixLinesJointBatchReader::new(get_data_file(RICH_LOGIC_TRACES_FILENAME), get_data_file(PTS_FILENAME), get_data_file(CTS_FILENAME), BATCH_SIZE)?;
    // let io_functions = load_io_functions(get_data_file(IO_FUNCTIONS_FILENAME), &mut joint_logic_traces, &pts_ind, &cts_ind)?;

    // let predict = Predictor::from_functions(io_functions, functions, logic_hg);

    // // Test vectors
    // let pt = vec![0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF]; 
    // let ct = vec![0x69, 0xC4, 0xE0, 0xD8, 0x6A, 0x7B, 0x04, 0x30, 0xD8, 0xCD, 0xB7, 0x80, 0x70, 0xB4, 0xC5, 0x5A];
    // let k =  vec![0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf];

    // println!("PT: {}", vec2str(&pt));
    // let ct_computed = predict.predict(&pt)?;

    // println!("K: {}", vec2str(&k));

    // println!("Predicted CT: {}", vec2str(&ct_computed));
    // println!("True CT: {}", vec2str(&ct));
    
    // println!("SUCCESS: {}", ct == ct_computed);

    Ok(())
}
