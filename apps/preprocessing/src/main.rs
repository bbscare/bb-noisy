
use bbscare_libs::loaders::trace_matrix::{TraceMatrixLinesBatchReader, TraceMatrixLinesBatchWriter};
use anyhow;
use std::env;
use rayon::iter::IntoParallelRefIterator;
use rayon::iter::ParallelIterator;

use indicatif::{ProgressBar};

const BATCH_SIZE: usize = 10000;
const RAW_TRACES_FILENAME: &str = "traces_raw_1M.binu8";
const PROCESSED_TRACES_FILENAME: &str = "traces_processed_1M.binu8";
const REDUCTION_FACTOR: usize = 5;
const AVERAGE_WINDOW: usize = 10;

fn get_data_file(filename: &str) -> String {
    let data_folder: String = match env::var("DATA") {
        Ok(d) => d,
        Err(_) => "../../data".to_owned()
    };
    return data_folder + "/" + filename;
}


fn mean(raw_traces: &mut TraceMatrixLinesBatchReader) -> anyhow::Result<Vec<u8>> {
    raw_traces.reset()?;
    let mut mean_vec: Vec<u64> = vec![0; raw_traces.traces_len];
    assert!((raw_traces.traces_count as u64) < (u64::MAX / 256u64));

    let bar = ProgressBar::new((raw_traces.traces_count / BATCH_SIZE) as u64);
    while let Ok(batch) = raw_traces.read_batch() {
        for row in batch.iter() {
            for (i, v) in row.iter().enumerate() {
                mean_vec[i] += *v as u64;
            }
        }
        bar.inc(1);
    }
    bar.finish();

    let mut mean_vec_u8: Vec<u8> = vec![0; raw_traces.traces_len];

    for i in 0..mean_vec.len() {
        mean_vec_u8[i] /= ((mean_vec[i] / raw_traces.traces_count as u64) & 0xFF) as u8;
    }

    raw_traces.reset()?;
    Ok(mean_vec_u8)
}

fn preprocess_rolling_average(raw_traces: &mut TraceMatrixLinesBatchReader) -> anyhow::Result<()> {
    let mean = mean(raw_traces)?;
    raw_traces.reset()?;
    let preprocessed_len = (raw_traces.traces_len - AVERAGE_WINDOW) / REDUCTION_FACTOR ;
    let mut processed_traces = TraceMatrixLinesBatchWriter::new(get_data_file(PROCESSED_TRACES_FILENAME), preprocessed_len, raw_traces.traces_count)?;

    let bar = ProgressBar::new((raw_traces.traces_count / BATCH_SIZE) as u64);
    while let Ok(batch) = raw_traces.read_batch() {
        let batch_to_write: Vec<Vec<u8>> = batch.par_iter().map(|row| {
            let mut new_row: Vec<u8> = vec![0; preprocessed_len];
            for i in (0..(raw_traces.traces_len - AVERAGE_WINDOW)).step_by(REDUCTION_FACTOR) {
                let mut av: u64 = 0u64;
                for off in 0..AVERAGE_WINDOW {
                    av += (row[i+off] - mean[i+off]) as u64;
                }
                new_row[i/REDUCTION_FACTOR] = (av / (AVERAGE_WINDOW as u64)) as u8;
            }

            new_row            
        }).collect();

        processed_traces.write_batch(&batch_to_write)?;
        bar.inc(1);
    }
    bar.finish();

    Ok(())
}

fn preprocess_centering(raw_traces: &mut TraceMatrixLinesBatchReader) -> anyhow::Result<()> {
    let mean = mean(raw_traces)?;
    raw_traces.reset()?;
    let preprocessed_len = raw_traces.traces_len;
    let mut processed_traces = TraceMatrixLinesBatchWriter::new(get_data_file(PROCESSED_TRACES_FILENAME), preprocessed_len, raw_traces.traces_count)?;

    let bar = ProgressBar::new((raw_traces.traces_count / BATCH_SIZE) as u64);
    while let Ok(batch) = raw_traces.read_batch() {
        let batch_to_write: Vec<Vec<u8>> = batch.par_iter().map(|row| {
            let mut new_row: Vec<u8> = vec![0; preprocessed_len];
            for i in 0..raw_traces.traces_len {
                new_row[i] = row[i] - mean[i];
            }

            new_row            
        }).collect();

        processed_traces.write_batch(&batch_to_write)?;
        bar.inc(1);
    }
    bar.finish();

    Ok(())
}

fn main() -> anyhow::Result<()> {

    let mut raw_traces = TraceMatrixLinesBatchReader::new(get_data_file(RAW_TRACES_FILENAME), BATCH_SIZE)?;
    assert!(raw_traces.traces_len % REDUCTION_FACTOR == 0);

    

    preprocess_centering(&mut raw_traces)?;

    Ok(())
}
 