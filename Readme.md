# Black-Box SCARE

*The clean code version of this scenario is still a work-in-progress*
The code behind the paper's noisy scenario is "research code" (julia language).
We do not think that it is clean enough for public release at this time, but feel free to contact us to get access to the code.