with import <nixpkgs> {};


let
     pkgs = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "nixos-22-11";
        url = "https://github.com/nixos/nixpkgs/";
        ref = "refs/heads/nixos-22.11";
        rev = "ab1254087f4cdf4af74b552d7fc95175d9bdbb49";                                           
     }) {};                            

    rvem = (import ../packages/rvem.nix);                            
in

let
    lascar_pkg = ps: ps.callPackage ./lascar.nix {};
    pythonEnv = pkgs.python310.withPackages (ps: [
    ps.matplotlib
    ps.tqdm
    ps.numpy
    ps.keras
    ps.tensorflow
    ps.termcolor
    (lascar_pkg ps)
    ps.scikit-learn
    ps.joblib
  ]);
in

stdenv.mkDerivation {
    name = "bb-noisy-env";

    DATA = toString ../data;
    LIBS = toString ../libs;
    AES = toString ../targeted-algorithms/aes/aes.s;
    TWINE = toString ../targeted-algorithms/twine/twine.s;

    # The packages in the `buildInputs` list will be added to the PATH in our shell
    nativeBuildInputs = with pkgs; [
        rvem
        cargo
        rustc
        pythonEnv
    ];


    shellHook = ''
        mkdir -p data
    '';
}