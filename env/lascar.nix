{ lib
, buildPythonPackage
, pythonOlder
, fetchFromGitHub
, fetchPypi
, numpy
, scipy
, pytestrunner
, pytest
, setuptools
, keras
, click
, h5py
, matplotlib
, numba
, progressbar2
, psutil
# , pyqt5
, scikit-learn
, tensorflow
, vispy
}:

buildPythonPackage rec {
    pname = "lascar";
    commit = "2638f43";
    version = "1.1";

    disabled = pythonOlder "3.8";

    # src = fetchFromGitHub {
    #     owner = "Ledger-Donjon";
    #     repo = "${pname}";
    #     rev = "${commit}";
    #     hash = "sha256-D9ju6iz58BH/5ZU2U5OOheUstwgjz28gkmpvOK4+HOc=";
    # };

    src = fetchPypi {
        inherit pname version;
        hash = "sha256-al137jZx687O6chzb9wkk4bXGIHNF9otjAhR3dq+g8M=";
    };

    propagatedBuildInputs = [
        setuptools
        numpy
        scipy
        pytestrunner
        pytest
        keras
        click
        h5py
        matplotlib
        numba
        progressbar2
        psutil
        # pyqt5
        scikit-learn
        tensorflow
        vispy
    ];

    doCheck = false;

    pythonImportsCheck = [ "lascar" ];
    checkInputs = ["pytestCheckHook"];

    meta = with lib; {
        description = "Lascar Side-Channel Analysis Library";
        homepage = "https://github.com/Ledger-Donjon/lascar";
        license = licenses.gpl3;
    };
}
