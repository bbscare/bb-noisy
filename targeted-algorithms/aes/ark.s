/*
 * File: ark.bad
 * Project: aes
 * Created Date: Friday November 29th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 14th February 2020 3:03:12 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

#[global]
@ark:
    //load key words and xor with State registers
    t0 <-t0 [0x1010];
    s0 <- s0 ^ t0;
    t0 <-t0 [0x1014];
    s1 <- s1 ^ t0;
    t0 <-t0 [0x1018];
    s2 <- s2 ^ t0;
    t0 <-t0 [0x101C];
    s3 <- s3 ^ t0;
    return;