/*
 * File: twine.s
 * Project: twine
 * Created Date: Wednesday June 30th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 3rd March 2022 2:50:44 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

import "s.s";
import "p.s";
import "key_sch.s";

//memory layout
// @0x1000: plaintext bytes up to 0x1007
// @0x1010: key bytes up to 0x101B
// @0x1020: ciphertext bytes up to 0x1027

// @0x1030: tmp nibbles up to 0x103F
// @0x1040: key nibbles up to 0x1053
// @0x1060: ciphertext/state nibbles up to 0x106F
// @0x1070: round key nibbles up to 0x1077


// State is stored on s0, s1
//#[address(0xA0000)]
// @cipher_data:
//def pt1: u32 = 0x01234567;
//def pt2: u32 = 0x89ABCDEF;

//def pt1: u32 = 0x67452301;
//def pt2: u32 = 0xEFCDAB89;


#[address(0xB0000)]
@key_data:

//def k1: u32 = 0x00112233;
//def k2: u32 = 0x44556677;
//def k3: u32 = 0x88990000;

def k1: u32 = 0x33221100;
def k2: u32 = 0x77665544;
def k3: u32 = 0x00009988;


//init
#[address(0x8000)]
@entry:

//init stack
sp <- 0x3000;

// set fixed plaintext in memory
//t0 <-t1 [&pt1]; //load pt1 in t0
t0 <- CSR[0x123]; //init with prng
[0x1000] <-t1 t0;//store t0 at 0x1000
//t0 <-t1 [&pt2];
t0 <- CSR[0x123]; //init with prng
[0x1004] <-t1 t0;

// set key in memory
t0 <-t1 [&k1];
[0x1010] <-t1 t0;
t0 <-t1 [&k2];
[0x1014] <-t1 t0;
t0 <-t1 [&k3];
[0x1018] <-t1 t0;

//convert key bytes to nibbles
a0 <- 0x1010;
a1 <- 0x1040;
a2 <- 10;
call &bytes2nibbles;

//convert plaintext bytes to nibbles in state
a0 <- 0x1000;
a1 <- 0x1060;
a2 <- 8;
call &bytes2nibbles;

//init work key
call &work2roundkey;

//round counter
s0 <- 0;
@enc_loop_start:

    // Substitution
    s1 <- 0;
    @s_loop_start:
    s2 <- s1 << 1; //2*j
    t1 <- 0x1060;
    t1 <- s2 + t1;
    t1 <- [t1]b;//X_2j
    t2 <- 0x1070;
    t2 <- s1 + t2;
    t2 <- [t2]b;//Rk_j

    a0 <- t1 xor t2;
    call &s;//a0 <- S(X_2j xor Rk_j)

    t1 <- 0x1061;
    t1 <- s2 + t1;
    t2 <- [t1]b;//X_2j+1
    t2 <- t2 xor a0;
    [t1]b <- t2;

    s1 <- s1 + 1;
    t0 <- 8;
    if (s1 <u t0) goto &s_loop_start;

    // Permutation
    s1 <- 0;
    @p_loop_start:

    a0 <- s1;
    call &p;
    t2 <- 0x1060;//source base
    t4 <- 0x1030;//destination base
    t1 <- s1 + t2;//source
    t2 <- a0 + t4;//destination
    t3 <- [t1]b;
    [t2]b <- t3;
    
    s1 <- s1 + 1;
    t0 <- 16;
    if (s1 <u t0) goto &p_loop_start;

    a0 <- 0x1060;
    a1 <- 0x1030;
    a2 <- 16;
    call &copy;

    a0 <- s0;
    call &key_round80;

s0 <- s0 + 1;
t0 <- 35;
if (s0 <u t0) goto &enc_loop_start;


//last round
// Substitution
s1 <- 0;
@send_loop_start:
s2 <- s1 << 1; //2*j
t1 <- 0x1060;
t1 <- s2 + t1;
t1 <- [t1]b;//X_2j
t2 <- 0x1070;
t2 <- s1 + t2;
t2 <- [t2]b;//Rk_j

a0 <- t1 xor t2;
call &s;//a0 <- S(X_2j xor Rk_j)

t1 <- 0x1061;
t1 <- s2 + t1;
t2 <- [t1]b;//X_2j+1
t2 <- t2 xor a0;
[t1]b <- t2;

s1 <- s1 + 1;
t0 <- 8 ;
if (s1 <u t0) goto &send_loop_start;



a0 <- 0x1060;
a1 <- 0x1020;
a2 <- 8;
call &nibbles2bytes;

s0 <-t0 [0x1020];
s1 <-t0 [0x1024];

//save output
output s0;
output s1;
CSR[0] <- x0;//halt with error

//a0 bytes address
//a1 nibbles address
//a2 byte length
@bytes2nibbles:

    t3 <- a0; //byte address
    t4 <- a1; //nibble address
    t5 <- a2; // loop counter
@bytes2nibbles_loop_start:
    t0 <- [t3]b; // read byte
    t3 <- t3 + 1;

    t1 <- t0 >> 4;
    t1 <- t1 & 0xF; //useless normally
    [t4]b <- t1;
    t4 <- t4 + 1;

    t1 <- t0 & 0xF;
    [t4]b <- t1;
    t4 <- t4 + 1;

    t5 <- t5 -1;
    t0 <- 0;
    if (t5 >u t0) goto &bytes2nibbles_loop_start;

    return;

//a0 nibbles address
//a1 bytes address
//a2 byte length
@nibbles2bytes:

t3 <- a0; //nibble address
t4 <- a1; //byte address
t5 <- a2; // loop counter
@nibbles2bytes_loop_start:
    t0 <- [t3]b;
    t1 <- [t3 + 1]b;

    t2 <- t0 << 4;
    t2 <- t2 or t1;

    [t4]b <- t2;

    t3 <- t3 + 2;
    t4 <- t4 + 1;

    t5 <- t5 -1;
    t0 <- 0;
    if (t5 >u t0) goto &nibbles2bytes_loop_start;

    return;

//a0 dest address
//a1 source address
//a2 byte length
@copy:
    a2 <- a2 - 1;
    t0 <- a0 + a2;
    t1 <- a1 + a2;

    t2 <- [t1]b;
    [t0]b <- t2;

    if (a2 >u zero) goto &copy;
    return;