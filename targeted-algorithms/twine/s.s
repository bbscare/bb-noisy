/*
 * File: s.s
 * Project: twine
 * Created Date: Wednesday June 30th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 30th June 2021 4:33:41 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

#[global]
@s://expecting input as a0, return as a0
    t1 <- &sbox;

    t0 <- a0 & 0xF;
    t0 <- t0 + t1;
    a0 <- [t0]b;

    return;
    

def sbox: [u8] = [0xC, 0x0, 0xF, 0xA, 0x2, 0xB, 0x9, 0x5, 0x8, 0x3, 0xD, 0x7, 0x1, 0xE, 0x6, 0x4];